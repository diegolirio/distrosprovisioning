#!/usr/bin/env bash
#IntelliJ
echo ".............................. *************************** .............................."
echo "INSTALLIG IntelliJ"
echo ".............................. *************************** .............................."

sudo wget https://download.jetbrains.com/idea/ideaIC-2017.1.4.tar.gz
sudo chmod +x ideaIC-2017.1.4.tar.gz
sudo tar xvzf ideaIC-2017.1.4.tar.gz -C /opt/
sudo chmod -R 775 /opt/idea-IC-171.4694.23/
sh /opt/idea-IC-171.4694.23/bin/idea.sh
sudo rm ideaIC-2017.1.4.tar.gz