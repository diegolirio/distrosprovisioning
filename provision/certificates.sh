#!/usr/bin/env bash

echo ".............................. *************************** .............................."
echo ".............................. *************************** .............................."
echo "LETS CONFIGURE THE CERTIFICATES"
echo ".............................. *************************** .............................."
echo ".............................. *************************** .............................."


cd /tmp/

wget http://gitlab.accesstage.com.br/Devops/DistrosProvisioning/raw/master/provision/certificates/ca.accesstage.com.br.crt

sudo chmod 775 ca.accesstage.com.br.crt

sudo cp /tmp/ca.accesstage.com.br.crt /usr/share/ca-certificates/

sudo update-ca-certificates

sudo cp /tmp/ca.accesstage.com.br.crt /etc/ssl/certs

sudo update-ca-certificates

sudo cp /tmp/ca.accesstage.com.br.crt /etc/ca-certificates/update.d

sudo update-ca-certificates

sudo cp /tmp/ca.accesstage.com.br.crt /usr/local/share/ca-certificates

sudo update-ca-certificates

sudo mkdir -p /etc/docker/certs.d/

sudo mkdir -p /etc/docker/certs.d/registry.accesstage.com.br

sudo mkdir -p /etc/docker/certs.d/registry.accesstage.com.br:5000

sudo cp /tmp/ca.accesstage.com.br.crt /etc/docker/certs.d/registry.accesstage.com.br

sudo update-ca-certificates

sudo cp /tmp/ca.accesstage.com.br.crt /etc/docker/certs.d/registry.accesstage.com.br:5000

sudo update-ca-certificates

sudo cp /tmp/ca.accesstage.com.br.crt /etc/docker/certs.d/

sudo update-ca-certificates