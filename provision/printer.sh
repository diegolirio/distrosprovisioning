#!/usr/bin/env bash

echo ".............................. *************************** .............................."
echo ".............................. *************************** .............................."
echo "LETS CONFIGURE THE PRINTER"
echo ".............................. *************************** .............................."
echo ".............................. *************************** .............................."

wget http://gdlp01.c-wss.com/gds/8/0100002708/16/linux-UFRII-drv-v331-uken.tar.gz

tar -xvzf linux-UFRII-drv-v331-uken.tar.gz

cd linux-UFRII-drv-v331-uken/64-bit_Driver/Debian/

sudo dpkg -i cndrvcups-common_3.71-1_amd64.deb

sudo dpkg -i cndrvcups-ufr2-uk_3.31-1_amd64.deb

sudo apt -y -qq install libstdc\+\+6:i386

sudo service cups restart

sudo apt -y -qq install cups-backend-bjnp

sudo apt -y -qq install libxml2:i386 libjpeg62:i386 libstdc++6:i386

sudo service cups restart
