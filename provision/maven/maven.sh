#!/usr/bin/env bash

echo ".............................. *************************** .............................."
echo "INSTALLIG MAVEN"
echo ".............................. *************************** .............................."
sudo apt-get -y -qq install maven


echo ".............................. *************************** .............................."
echo "CONFIGURING SETTINGSXML"
echo ".............................. *************************** .............................."
mkdir ~/.m2
sleep 5
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cp $parent_path/provision/maven/settings.xml ~/.m2

