#!/usr/bin/env bash

echo ".............................. *************************** .............................."
echo "INSTALLIG DBEAVER"
echo ".............................. *************************** .............................."

echo "  ____    ____                                       "
echo " |  _ \  | __ )    ___    __ _  __   __   ___   _ __ "
echo " | | | | |  _ \   / _ \  / _  | \ \ / /  / _ \ | '__|"
echo " | |_| | | |_) | |  __/ | (_| |  \ V /  |  __/ | |   "
echo " |____/  |____/   \___|  \__,_|   \_/    \___| |_|   "
sudo wget http://dbeaver.jkiss.org/files/dbeaver-ee-latest-linux.gtk.x86_64.tar.gz
sudo chmod +x dbeaver-ee-latest-linux.gtk.x86_64.tar.gz
mkdir /opt && chmod -R 775 /opt
sudo tar xvzf dbeaver-ee-latest-linux.gtk.x86_64.tar.gz -C /opt/
sudo chmod -R 775 /opt/dbeaver/
sudo rm dbeaver-ee-latest-linux.gtk.x86_64.tar.gz

echo ".............................. *************************** .............................."
echo "CONFIGURING SHORTCUT"
echo ".............................. *************************** .............................."
sleep 5
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
sudo cp $parent_path/install/dbeaver/dbeaver.desktop /usr/share/applications/
sudo chmod 644 /usr/share/applications/dbeaver.desktop