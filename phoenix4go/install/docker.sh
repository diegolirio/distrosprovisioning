#!/usr/bin/env bash

#sudo curl -fsSL https://get.docker.com/ | sh

echo "  ____       U  ___ u    ____     _  __    U _____ u    ____     "
echo " |  _ \       \/ _ \/ U / ___|   | |/ /    \| ___ |/ U |  _ \ u  "
echo "/| | | |      | | | | \| | u     | ' /      |  _|     \| |_) |/  "
echo "U| |_| |\ .-,_| |_| |  | |/__  U/| . \\u    | |___     |  _ <    "
echo " |____/ u  \_)-\___/    \____|   |_|\_\     |_____|    |_| \_\   "
echo "  |||_          \\     _// \\  ,-,>> \\,-.  <<   >>    //   \\_  "
echo " (__)_)        (__)   (__)(__)  \.)   (_/  (__) (__)  (__)  (__) Ubuntu 18.04"


echo "........................................................................................."
#echo " apt install docker.io -y "
echo "........................................................................................."

#apt install docker.io -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce -y
sudo service docker start

echo "........................................................................................."
echo " systemctl status docker "
echo "........................................................................................."
sudo systemctl status docker 

echo "........................................................................................."
echo " execute docker as non user root "
echo "........................................................................................."
sudo groupadd docker
sudo usermod -aG docker $USER
# Check
cat /etc/group | grep docker
# login
su - $USER
docker ps



