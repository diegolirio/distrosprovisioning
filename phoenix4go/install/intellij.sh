#!/usr/bin/env bash
#IntelliJ
echo ".............................. *************************** .............................."
echo "INSTALLIG IntelliJ"
echo ".............................. *************************** .............................."

wget https://download.jetbrains.com/idea/ideaIC-2017.3.4.tar.gz
sudo chmod +x ideaIC-2017.3.4.tar.gz
sudo tar xvzf ideaIC-2017.3.4.tar.gz -C /opt/
sudo chmod -R 775 /opt/idea-IC-173.4548.28/	
sh /opt/idea-IC-173.4548.28/bin/idea.sh
sudo rm ideaIC-2017.3.4.tar.gz