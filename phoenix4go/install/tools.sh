#!/usr/bin/env bash
#Tools

echo ".-. .-.  .-. .-.  _____   ,-.  ,---.   "
echo "| | | |  |  \| | /___  /  |(|  | .-.\  "
echo "| | | |  |   | |    / /)  (_)  | |-' ) "
echo "| | | |  | |\  |   / /(_) | |  | |--'  "
echo "|  -')|  | | |)|  / /___  | |  | |     "
echo " ---(_)  /(  (_) (_____/   -'  /(      "
echo "        (__)                  (__)     "
sudo apt-get install -y -qq unzip

echo "                _   _         "
echo " __  __   ___  | | (_)  _ __  "
echo " \ \/ /  / __| | | | | | '_ \ "
echo "  >  <  | (__  | | | | | |_) |"
echo " /_/\_\  \___| |_| |_| | .__/ "
echo "                       |_|          "
sudo apt-get install -y -qq xclip

echo "        __          "
echo "___  __|__|  _____  "
echo "\  \/ /|  | /     \ "
echo " \   / |  ||  Y Y  \ "
echo "  \_/  |__||__|_|  /"
echo "                 \/ "
sudo apt-get install -y -qq vim

echo "       _..._                                 "
echo "    .-'_..._''.                       .---.  "
echo "  .' .'      '.\                      |   |  "
echo " / .'                                 |   |  "
echo ". '                          .-,.--.  |   |  "
echo "| |                          |  .-. | |   |  "
echo "| |                 _    _   | |  | | |   |  "
echo ". '                | '  / |  | |  | | |   |  "
echo " \ '.          .  .' | .' |  | |   -  |   |  "
echo "  \   ._____.- /  /   /   /  / /      /   \  "
echo "    -.______ /  |    '.  |   | |       ---   "
echo "                '   .'|  '   \_\            "
echo "                   -'   --'                 "
sudo apt-get install -y -qq curl

echo "Make is being installed..."
sudo apt-get -y install make

