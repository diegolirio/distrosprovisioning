#!/usr/bin/env bash

echo ""
echo "  __  __       _       __     __   U _____ u   _   _     "
echo "U|' \/ '|u U  / \  u   \ \   / /u  \| ___ |/  | \ | |    "
echo "\| |\/| |/  \/ _ \/     \ \ / //    |  _|    <|  \| |>   "
echo " | |  | |   / ___ \     /\ V /_,-.  | |___   U| |\  |u   "
echo " |_|  |_|  /_/   \_\   U  \_/-(_/   |_____|   |_| \_|    "
echo "<<,-,,-.    \\    >>     //         <<   >>   ||   \\,-. "
echo " (./  \.)  (__)  (__)   (__)       (__) (__)  (_ )  (_/  "
sudo apt-get -y -qq install maven

#echo ".............................. *************************** .............................."
#echo "CONFIGURING SETTINGSXML"
#echo ".............................. *************************** .............................."
#mkdir ~/.m2
#sleep 5
#parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
#cp $parent_path/provision/maven/settings.xml ~/.m2

