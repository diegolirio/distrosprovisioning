#!/usr/bin/env bash

#echo "   _                      "
#echo "  (_)  __ _ __   __  __ _ "
#echo "  | | / _  |\ \ / / / _  |"
#echo "  | || (_| | \ V / | (_| |"
#echo " _/ | \__,_|  \_/   \__,_|"
#echo "|__/                      openjdk-21 with sdkman"

source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install java 21.0.6-graal

echo 'To see more access:'
echo 'https://medium.com/devspoint/gerenciando-as-muitas-vers%C3%B5es-do-java-com-sdk-man-a8368c6b2926'