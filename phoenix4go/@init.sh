#!/usr/bin/env bash

sudo apt-get -y -qq upgrade
sudo apt-get -y -qq update

#+ Tools
sh ./install/tools.sh

#+ JDK
#sh ./install/java.sh

#+ SDK Man
sh sdk.sh

#+ Maven
sh ./install/maven/maven.sh

#+ Git
#sh ./install/git.sh

#+ DBeaver // TODO error
sudo chmod -R +x ./install/dbeaver/
sh ./install/dbeaver/dbeaver.sh

#+ Gimp // TODO error
sh ./install/gimp.sh

#+ Virtualbox
sh ./install/virtualbox.sh

#+ NPM
sh ./install/npm.sh

#+ IntelliJ // TODO no install 
sh ./install/intellij.sh

#+ Docker
sh ./install/docker.sh

#+ Docker-compose
sh ./install/docker-compose.sh

# TODO VS Code

# Docker mongodb
sh ./install/mongo.sh

# Docker MySQL // TODO no install 
cd install/mysql/
sh ./mysql.sh
cd ../..

#+ Oh My ZShell
sh ./install/terminator.sh

#+ Oh My ZShell
sh ./install/ohmyzshell.sh
